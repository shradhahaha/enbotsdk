Setup ENBotSDK in iOS

Following steps to integrate ENBotSDK into your iOS App:

<b>01</b>

Initialize using pod
In your podfile add below entry inside your project target

Add following to podfile
    <br>pod ‘ENChatBot’

post_install do |installer|
    <br>installer.pods_project.targets.each do |target|
      <br>target.build_configurations.each do |config|
        <br>config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
      <br>end
    <br>end
  <br>end
And then
    <br>pod install

<b>02</b>

Add following framework to iOS project if not added already:
   <br> AVFoundation
   <br> UIKit

<b>03</b>

Use ENChatBot

Import ENChatBot from where you want to initiate chat screen:

    import ENChatBot

Use bot_key and bot_name to initialize ENChatBotConfig class

    let chatConfig = ENChatBotConfig.init(botKey: "bot_key", 
    botName: "bot_name", 
    brandingKey: "branding_key",
    userId: “anyUserId”, 
    chatHistorySize: 100,
    showDone: true/false, 
    language = .default/.rtl, 
    headerTitleFont: UIFont(name: "Helvetica", size: 15),
    headerDescriptionFont: UIFont(name: "Helvetica", size: 15), 
    sendMessageButtonIcon: UIImage(named:”your_image”),
    userAttributes: ["attr_name":"attr_value"])

Initialize ChatController :
    let chatController = ENChatBotViewController.init(chatConfig: chatConfig, callBackDelegate: self)
     // self: UIViewController which confirms to “ENChatBotCallBack” protocol

    public protocol ENChatBotCallBack {
        func didCloseENChatBot(_ withInfo: Any?)
    }

From current controller:
- push chat controller
    self.navigationController?.pushViewController(chatController, animated: true)
- present chat controller
    self.present(UINavigationController(rootViewController: chatController), animated: true, completion: nil)
- popup chat controller
    Create a “containerView”
    let cvc = UINavigationController(rootViewController: chatController)
    cvc.view.frame = containerView.bounds
    containerView.addSubview(cvc.view)
    self.addChild(cvc)
    cvc?.didMove(toParent: self)

In didCloseENChatBot(_ withInfo: Any?) - (to close the bot) 
    cvc?.willMove(toParent: nil)   
    cvc?.view.removeFromSuperview()
    cvc?.removeFromParent()



Note: If you are serving images or media content(any of path node or carousel image) from non secure source(non https) then please add following to application’s info.plist file:

    <key>NSAppTransportSecurity</key>
    <dict>
        <key>NSAllowsArbitraryLoads</key>
    <true/>
    </dict>



Or add the respective domain url list as NSExceptionDomains.

For more details on setting up exception domains pls check:
https://developer.apple.com/library/content/documentation/General/Reference/InfoPlistKeyReference/Articles/CocoaKeys.html#//apple_ref/doc/uid/TP40009251-SW35
 


For uploading File from bot using Camera, Photo Library and Files,  add the following to application’s info.plist file:
    <key>LSSupportsOpeningDocumentsInPlace</key>
		<true/>
	<key>NSPhotoLibraryUsageDescription</key>
		<string>To upload image into bot</string>
	<key>NSCameraUsageDescription</key>
		<string>To upload picture in bot</string>

To support all the fonts, add the following keys to application’s info.plist file:
	<key>UIAppFonts</key>
	<array>
		<string>Calibri.ttf</string>
		<string>Roboto-Regular.ttf</string>
		<string>Roboto-Light.ttf</string>
		<string>Garamond.ttf</string>
		<string>Garamond Light.ttf</string>
		<string>IndieFlower-Regular.ttf</string>
	</array>

And add the corresponding ttf files in application


Please find below the demo app for chat sdk:

https://gitlab.com/en-sdk/enbotsdk/-/tree/SampleApp


Known Issues:
1. Doesn't work for XCode 12 GM Seed Simulator <br>
   <b>Workaround</b> : Go to your build settings and add User-defained settings called VALID_ARCHS and add value as x86_64, Also add value for build setting  "Excluded Architecture" as arm64. Please make sure to remove this when archiving. 


